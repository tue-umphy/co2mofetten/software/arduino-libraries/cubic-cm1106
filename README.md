# CUBIC CM1106 Arduino Library

This library provides a class to interface the CUBIC CM1106 CO2 sensor.

> ### Note
>
> The code is based on the [Theben CO2 Module Arduino
> Library](https://gitlab.com/tue-umphy/co2mofetten/arduino-libraries/ThebenCO2)

## Capabilities

- requesting the current software version
- requesting the current CO2 concentration in ppm
- performing a CO2 calibration (single-point offset)

## Examples

See the examples under the `examples` folder.

## Debugging

You can get the library to talk about every action it takes by setting the
compiler flag `CM1106_DEBUG` (e.g. set `build_flags = -DCM1106_DEBUG` if
you are using PlatformIO, I don't know how to do this in the Arduino IDE
directly, but you can always just do a `#define CM1106_DEBUG` in the
`CM1106.h` header file to accomplish this.)

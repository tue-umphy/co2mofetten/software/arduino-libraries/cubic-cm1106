#ifndef CM1106_h
#define CM1106_h

#include <Arduino.h>
#include <Wire.h>

#define DF(s)                                                                 \
  {                                                                           \
    Serial.print(F(s));                                                       \
  };
#define DP(s)                                                                 \
  {                                                                           \
    Serial.print(s);                                                          \
  };
#define DH(s)                                                                 \
  {                                                                           \
    DP("0x");                                                                 \
    Serial.print(s, HEX);                                                     \
  };

#ifdef CM1106_DEBUG
#define CM1106DBG(cmd)                                                        \
  {                                                                           \
    DP("CM1106: ");                                                           \
    cmd;                                                                      \
    Serial.println();                                                         \
    Serial.flush();                                                           \
  };
#else
#define CM1106DBG(cmd)
#endif

#define CM1106DS(s) CM1106DBG(DF(s))
#define CM1106DSN(s, n) CM1106DBG(DF(s); DP(n))
#define CM1106DSNS(s1, n, s2) CM1106DBG(DF(s1); DP(n); DF(s2))
#define CM1106DSNSN(s1, n1, s2, n2) CM1106DBG(DF(s1); DP(n1); DF(s2); DP(n2))
#define CM1106DSNSNS(s1, n1, s2, n2, s3)                                      \
  CM1106DBG(DF(s1); DP(n1); DF(s2); DP(n2); DF(s3))
#define CM1106DSNSNSH(s1, n1, s2, n2, s3, n3)                                 \
  CM1106DBG(DF(s1); DP(n1); DF(s2); DP(n2); DF(s3); DH(n3))
#define CM1106DSH(s, n) CM1106DBG(DF(s); DH(n))
#define CM1106DSHS(s1, n, s2) CM1106DBG(DF(s1); DH(n); DF(s2))
#define CM1106DSHSH(s1, n1, s2, n2) CM1106DBG(DF(s1); DH(n1); DF(s2); DH(n2))
#define CM1106DSNSH(s1, n1, s2, n2) CM1106DBG(DF(s1); DP(n1); DF(s2); DH(n2))
#define CM1106DSHSHS(s1, n1, s2, n2, s3)                                      \
  CM1106DBG(DF(s1); DH(n1); DF(s2); DH(n2); DF(s3))
#define CM1106DSHSNS(s1, n1, s2, n2, s3)                                      \
  CM1106DBG(DF(s1); DH(n1); DF(s2); DP(n2); DF(s3))

#define arraysize(a) (size_t)(sizeof(a) / sizeof(*a))

template<class SOURCE_TYPE, class TARGET_TYPE>
const TARGET_TYPE
cm1106bufferToUint(const SOURCE_TYPE buffer[],
                   const size_t startIndex = 0,
                   const bool highByteFirst = true)
{
  TARGET_TYPE value = 0;
  const size_t targetBytesPerSource =
    sizeof(TARGET_TYPE) / sizeof(SOURCE_TYPE);
  size_t i = highByteFirst ? 0 : (targetBytesPerSource - 1);
  for (size_t n = 0; n < targetBytesPerSource; n++) {
    size_t shift = (highByteFirst ? (targetBytesPerSource - 1 - i) : i) *
                   sizeof(SOURCE_TYPE) * 8;
    TARGET_TYPE add = (TARGET_TYPE)(buffer[i + startIndex]) << shift;
    value |= add;
    highByteFirst ? i++ : i--;
  }
  return value;
}

const int cm1106min_time_between_comm_ms = 0; // minimum time between comm.

const uint8_t cm1106_default_i2c_7bit_address = 0x31; // I2C address

const unsigned long cm1106response_timeout_ms = 100; // maximum comm time

enum class CM1106_CONTROL_BYTE : uint8_t
{
  READ_CO2 = 0x01,     // get current CO2 measurement
  ZERO_SETTING = 0x03, // zero setting
  ABC = 0x10,          // auto zero setting
  VERSION = 0x1E,      // software version
  DEVICE_CODE = 0x1F,  // device code
};

enum class CM1106_ABC_STATUS : uint8_t
{
  ON = 0x00,
  OFF = 0x02,
};

enum class CM1106_COMM_STATUS : uint8_t
{
  SUCCESS = 0x00,                 // communication was successful
  RESPONSE_CHECKSUM_ERROR = 0x01, // device responded invalid checksum
  RESPONSE_TIMEOUT = 0x02,        // device response took too long
  DIFFERENT_CONTROL = 0x03,       // device responded different control byte
  RESPONSE_ERROR = 0x04,          // device responded error
  IDLE = 0x05,                    // nothing done yet
  INVALID_RESPONSE = 0x06,        // invalid response
};

enum class CM1106_OP_STATUS : uint8_t
{
  PREHEATING =
    0x00, // during preheating (after powerup) the CO2 value is always 550
  NORMAL = 0x01,         // normal operation
  TROUBLE = 0x02,        // operating trouble
  OUT_OF_FS = 0x03,      // out of FS (whatever that means...)
  NOT_CALIBRATED = 0x04, // not calibrated
};

class CM1106
{
public:
  const bool begin(TwoWire& port = Wire,
                   uint8_t address = cm1106_default_i2c_7bit_address);
  bool present(void);
  const CM1106_COMM_STATUS getVersion(char version[]);
  const uint16_t getCO2(void);
  const uint16_t getCO2(CM1106_OP_STATUS& opStatus);
  const CM1106_COMM_STATUS getCO2(uint16_t& co2_ppm,
                                  CM1106_OP_STATUS& opStatus);
  const CM1106_COMM_STATUS zeroSetting(const uint16_t offset);
  const CM1106_COMM_STATUS getABC(bool& enable,
                                  uint8_t& cycle_days,
                                  uint16_t& value);
  const CM1106_COMM_STATUS setABC(bool enable,
                                  uint8_t cycle_days = 15,
                                  uint16_t value = 400);
  const bool success(const CM1106_COMM_STATUS);
  CM1106_COMM_STATUS last_comm_status = CM1106_COMM_STATUS::IDLE;

private:
  TwoWire* _port;
  uint8_t _address;
  unsigned long _millis_last_comm;
  CM1106_COMM_STATUS _communicate(const CM1106_CONTROL_BYTE control,
                                  uint8_t response[],
                                  const uint8_t response_len,
                                  const uint8_t data[] = {},
                                  const uint8_t data_len = 0);
  void _wait(void);
  uint8_t _calc_checksum(uint8_t buffer[], size_t len);
};

const bool
CM1106::begin(TwoWire& port, uint8_t address)
{
  _address = address; // remember the address
  if (_address >= 128) {
    address >>= 1;
    CM1106DSHSH("Warning: transformed 8-bit address ",
                address,
                " to 7-bit address ",
                _address);
  }
  _port = &port;  // remember the I2C port
  _port->begin(); // begin I2C port
  _millis_last_comm = 0;
  return this->present();
}

bool
CM1106::present(void)
{
  this->_wait();
  _port->beginTransmission(_address);
  return _port->endTransmission() == 0;
}

void
CM1106::_wait(void)
{
  while (millis() - this->_millis_last_comm < cm1106min_time_between_comm_ms)
    ;
}

const bool
CM1106::success(const CM1106_COMM_STATUS status)
{
  return status == CM1106_COMM_STATUS::SUCCESS;
}

uint8_t
CM1106::_calc_checksum(uint8_t buffer[], size_t len)
{
  uint8_t cs = 0; // init
  for (size_t i = 0; i < len; i++) {
    cs += buffer[i]; // update
  }
  cs *= -1; // final
  return cs;
}

CM1106_COMM_STATUS
CM1106::_communicate(const CM1106_CONTROL_BYTE control,
                     uint8_t response[],
                     const uint8_t response_len,
                     const uint8_t request_data[],
                     const uint8_t request_data_len)
{
  const uint8_t request_payload_len = 1 + request_data_len + 1;
  uint8_t request_payload[request_payload_len];
  uint8_t p = 0;
  request_payload[p++] = (uint8_t)control;
  for (uint8_t i = 0; i < request_data_len; i++) {
    request_payload[p++] = request_data[i];
  }
  while (this->_port->available()) {
#ifdef CM1106_DEBUG
    uint8_t tmp = this->_port->read();
    CM1106DSHS("Warning: discarding extra byte ", tmp, " from device");
#else
    this->_port->read();
#endif
  }
  CM1106DSHS(
    "beginTransmission(", this->_address, ") - preparing the request");
  Wire.beginTransmission(this->_address);
  for (uint8_t i = 0; i < p; i++) {
    CM1106DSHS("write(", request_payload[i], ") - filling buffer")
    this->_port->write(request_payload[i]);
  }
  this->_wait();
  CM1106DS("endTransmission() - actually sending the request");
  this->_port->endTransmission();
  this->_millis_last_comm = millis();
  this->_wait();
  unsigned long millis_before_request_from = millis();
  CM1106DSHSNS("requestFrom(",
               this->_address,
               ",",
               response_len,
               ") - request answer from device");
  this->_port->requestFrom(this->_address, response_len);
  CM1106DS("waiting for response...");
  while (true) {
    if (this->_port->available()) {
      break;
    }
    if (millis() - millis_before_request_from > cm1106response_timeout_ms) {
      CM1106DS("Warning: response took too long.");
      this->last_comm_status = CM1106_COMM_STATUS::RESPONSE_TIMEOUT;
      return this->last_comm_status;
    }
  }
  uint8_t r = 0;
  while (true) {
    if (r >= response_len) {
      CM1106DSNS("Received all ", response_len, " response bytes");
      while (this->_port->available()) {
#ifdef CM1106_DEBUG
        uint8_t tmp = this->_port->read();
        CM1106DSHS("Warning: discarding extra byte ", tmp, " from device");
#else
        this->_port->read();
#endif
      }
      break;
    }
    if (millis() - millis_before_request_from > cm1106response_timeout_ms) {
      CM1106DS("Warning: response took too long.");
      this->last_comm_status = CM1106_COMM_STATUS::RESPONSE_TIMEOUT;
      return this->last_comm_status;
    }
    if (this->_port->available()) {
      uint8_t tmp = this->_port->read();
      response[r++] = tmp;
      CM1106DSHS("Received byte ", tmp, " from device");
    }
  }
  this->_millis_last_comm = millis();
  if (response[0] != (uint8_t)control) {
    this->last_comm_status = CM1106_COMM_STATUS::DIFFERENT_CONTROL;
    return this->last_comm_status;
  }
  uint8_t cs_response = this->_calc_checksum(response, r - 1);
  if (cs_response == response[r - 1]) {
    CM1106DSHS("Checksum ", cs_response, " is OK");
  } else {
    CM1106DSHSHS("Warning: checksum error (device says ",
                 response[r - 1],
                 ", we calculated ",
                 cs_response,
                 ")");
    this->last_comm_status = CM1106_COMM_STATUS::RESPONSE_CHECKSUM_ERROR;
    return this->last_comm_status;
  }
  this->last_comm_status = CM1106_COMM_STATUS::SUCCESS;
  return this->last_comm_status;
}

const CM1106_COMM_STATUS
CM1106::getVersion(char version[])
{
  uint8_t response[12];
  CM1106_COMM_STATUS status = this->_communicate(
    CM1106_CONTROL_BYTE::VERSION, // data available control byte
    response,                     // response buffer
    arraysize(response)           // response buffer length
  );
  if (status == CM1106_COMM_STATUS::SUCCESS) {
    uint8_t i = 0;
    for (uint8_t r = 1; r < 11; r++) {
      version[i++] = response[r];
    }
  }
  return status;
}

const CM1106_COMM_STATUS
CM1106::getCO2(uint16_t& co2_ppm, CM1106_OP_STATUS& opStatus)
{
  uint8_t response[5];
  CM1106_COMM_STATUS status =
    this->_communicate(CM1106_CONTROL_BYTE::READ_CO2, // CO2 control byte
                       response,                      // response buffer
                       arraysize(response)            // response buffer length
    );
  co2_ppm = ~(uint16_t)0; // start with 0xFFFFFFFF (== error)
  if (status == CM1106_COMM_STATUS::SUCCESS) {
    co2_ppm = cm1106bufferToUint<uint8_t, uint16_t>(response, 1, true);
  }
  switch (response[3]) {
    case (uint8_t)CM1106_OP_STATUS::PREHEATING:
      opStatus = CM1106_OP_STATUS::PREHEATING;
      break;
    case (uint8_t)CM1106_OP_STATUS::NORMAL:
      opStatus = CM1106_OP_STATUS::NORMAL;
      break;
    case (uint8_t)CM1106_OP_STATUS::TROUBLE:
      opStatus = CM1106_OP_STATUS::TROUBLE;
      break;
    case (uint8_t)CM1106_OP_STATUS::OUT_OF_FS:
      opStatus = CM1106_OP_STATUS::OUT_OF_FS;
      break;
    case (uint8_t)CM1106_OP_STATUS::NOT_CALIBRATED:
      opStatus = CM1106_OP_STATUS::NOT_CALIBRATED;
      break;
    default:
      CM1106DSH("WARNING: unknown operating status byte ", response[3]);
  }
  return status;
}

const uint16_t
CM1106::getCO2(CM1106_OP_STATUS& opStatus)
{
  uint16_t co2_ppm;
  while (not success(getCO2(co2_ppm, opStatus)))
    ;
  return co2_ppm;
}

const uint16_t
CM1106::getCO2(void)
{
  CM1106_OP_STATUS opStatus;
  uint16_t co2_ppm;
  while (not success(getCO2(co2_ppm, opStatus)))
    ;
  return co2_ppm;
}

const CM1106_COMM_STATUS
CM1106::zeroSetting(const uint16_t offset)
{
  uint8_t request[2] = { highByte(offset), lowByte(offset) };
  uint8_t response[4];
  CM1106_COMM_STATUS status =
    this->_communicate(CM1106_CONTROL_BYTE::ZERO_SETTING, // CO2 control byte
                       response,                          // response buffer
                       arraysize(response), // response buffer length
                       request,             // request data
                       arraysize(request)   // request data length
    );
  return status;
}

const CM1106_COMM_STATUS
CM1106::getABC(bool& enabled, uint8_t& cycle_days, uint16_t& value)
{
  uint8_t response[8];
  CM1106_COMM_STATUS status =
    this->_communicate(CM1106_CONTROL_BYTE::ABC, // CO2 control byte
                       response,                 // response buffer
                       arraysize(response)       // response buffer length
    );
  if (status == CM1106_COMM_STATUS::SUCCESS) {
    enabled = response[2] == (uint8_t)CM1106_ABC_STATUS::ON;
    cycle_days = response[3];
    value = (response[4] << 8) | response[5];
  }
  return status;
}

const CM1106_COMM_STATUS
CM1106::setABC(bool enable, uint8_t cycle_days, uint16_t value)
{
  uint8_t response[8];
  uint8_t request[6] = {
    100,                                    // reserved
    enable ? (uint8_t)0x00 : (uint8_t)0x02, // 0 = on, 2 = off
    cycle_days,                             // calibration period in days
    highByte(value),                        // calibration value high byte
    lowByte(value),                         // calibration value low byte
    100                                     // reserved
  };
  CM1106_COMM_STATUS status =
    this->_communicate(CM1106_CONTROL_BYTE::ABC, // CO2 control byte
                       response,                 // response buffer
                       arraysize(response),      // response buffer length
                       request,                  // request buffer
                       arraysize(request)        // request buffer length
    );
  if (status == CM1106_COMM_STATUS::SUCCESS) {
    for (size_t i = 0; i < arraysize(request); i++) {
      if (response[i + 1] != request[i]) {
        return CM1106_COMM_STATUS::INVALID_RESPONSE;
      }
    }
  }
  return status;
}

#endif

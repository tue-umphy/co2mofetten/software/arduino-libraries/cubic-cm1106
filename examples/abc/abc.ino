/*
Sketch to query the CO2 concentration from a CUBIC CM1106 sensor

Wiring
======

Arduino                    CM1106
                                                __________________
     5V ------------------ 5V                  |######            |.
    GND ------------------ GND                .|######   CM1106   |. SELECT
       `------------------ SELECT             .|###### (top view) |. SCL
    SDA ------------------ SDA            GND .|######            |. SDA
    SCL ------------------ SCL            5V  .|__________________|.
*/

#include <CM1106.h>

CM1106 cm1106;

void
setup()
{
  Serial.begin(
#if CM1106_DEBUG
    115200
#else  // #if CM1106_DEBUG
    9600
#endif // #if CM1106_DEBUG
  );
  if (not cm1106.begin())
    Serial.println("No CM1106 found. Check wiring.");
  char version[11] = { 0 };
  Serial.println("Querying software version...");
  while (not cm1106.success(cm1106.getVersion(version)))
    ;
  Serial.print("CM1106 software version: ");
  Serial.println(version);
}

void
loop()
{
  bool enabled;
  uint8_t cycle;
  uint16_t value;
  Serial.println("Retrieving ABC status...");
  while (true) {
    CM1106_COMM_STATUS status = cm1106.getABC(enabled, cycle, value);
    if (cm1106.success(status)) {
      Serial.println("Getting ABC status worked!");
      Serial.print("ABC is ");
      Serial.println(enabled ? "enabled" : "disabled");
      if (enabled) {
        Serial.print("Cycle is ");
        Serial.print(cycle);
        Serial.println(" days");
        Serial.print("value is ");
        Serial.print(value);
        Serial.println(" ppm");
      }
      break;
    } else {
      Serial.print("WARNING: Error code 0x");
      Serial.println((uint8_t)status, HEX);
    }
  }
  delay(1000);
  Serial.print(enabled ? "Disabling" : "Enabling");
  Serial.println(" ABC...");
  while (true) {
    CM1106_COMM_STATUS status = cm1106.setABC(not enabled, cycle, value);
    if (cm1106.success(status)) {
      Serial.println("Toggling ABC worked!");
      break;
    } else {
      Serial.print("WARNING: Error code 0x");
      Serial.println((uint8_t)status, HEX);
    }
  }
  delay(1000);
  uint16_t co2_ppm = cm1106.getCO2();
  Serial.print("CM1106 CO2 concentration: ");
  Serial.print(co2_ppm);
  Serial.println(" ppm");
  delay(1000);
}

/*
Sketch to query the CO2 concentration from a CUBIC CM1106 sensor

Wiring
======

Arduino                    CM1106
                                                __________________
     5V ------------------ 5V                  |######            |.
    GND ------------------ GND                .|######   CM1106   |. SELECT
       `------------------ SELECT             .|###### (top view) |. SCL
    SDA ------------------ SDA            GND .|######            |. SDA
    SCL ------------------ SCL            5V  .|__________________|.
*/

#include <CM1106.h>

CM1106 cm1106;

void
setup()
{
  Serial.begin(
#if CM1106_DEBUG
    115200
#else  // #if CM1106_DEBUG
    9600
#endif // #if CM1106_DEBUG
  );
  if (not cm1106.begin())
    Serial.println("No CM1106 found. Check wiring.");
  char version[11] = { 0 };
  Serial.println("Querying software version...");
  while (not cm1106.success(cm1106.getVersion(version)))
    ;
  Serial.print("CM1106 software version: ");
  Serial.println(version);
  Serial.println("Performing zeroSetting to 400ppm...");
  while (not cm1106.success(cm1106.zeroSetting(400)))
    ;
  Serial.println("Successfully performed zeroSetting to 400ppm");
}

void
loop()
{
  uint16_t co2_ppm = cm1106.getCO2();
  Serial.print("CM1106 CO2 concentration: ");
  Serial.print(co2_ppm);
  Serial.println(" ppm");
  delay(1000);
}
